#include <iostream>

#define _GNU_SOURCE
#include <dlfcn.h>

extern "C" {
    void __cyg_profile_func_enter(void* func_address, void* call_site);
    void __cyg_profile_func_exit(void* func_address, void* call_site);
}

// dladdrを用いて関数アドレスから関数シンボルへ変換
const char* addr2name(void* address) {
    Dl_info dli;
    if (dladdr(address, &dli) != 0
        && dli.dli_sname != 0) {
        printf("%s\n", dli.dli_sname);
        return dli.dli_sname;
    }
    return 0;
}

void __cyg_profile_func_enter(void* func_address, void* call_site) {
    printf("%p\n", func_address) ;
    const char* func_name = addr2name(func_address); 
    if (func_name) {
        std::cout << "simple tracer: enter - " << func_name << std::endl;
    }
}

void __cyg_profile_func_exit(void* func_address, void* call_site) {
    printf("%p\n", func_address) ;
    const char* func_name = addr2name(func_address); 
    if (func_name) {
        std::cout << "simple tracer: exit  - " << func_name << std::endl;
    }
}
