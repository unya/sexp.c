// g++ casadi_trace.cpp -std=c++11 -liberty -shared -fPIC -o libmytrace.so -liberty -ldl

#include <stdio.h>
#include <time.h>
#include <dlfcn.h>
#include <libiberty/demangle.h>
#include <mutex>

// csplit trace.out "/Integrator::eval/" {3

static std::mutex mymtx;

//static std::map<std::string, std::string> demangling_map;

std::string demangle(const std::string& s) {
  char* ret = cplus_demangle(s.c_str(), 0);
  if (ret) return ret;
  return s;
}

std::string demangle_lookup(const std::string& s) {
  return demangle(s);
 /** static std::unordered_map<std::string, std::string> demangling_map;
  auto it = demangling_map.find(s);
  if (it==demangling_map.end()) {
    demangling_map[s] = demangle(s);
    return demangling_map[s];
  } else {
    return it->second;
  }*/
}

extern "C" {


static bool instrumenting;
static int counter;

void __cyg_profile_func_enter (void *this_fn, void *call_site) __attribute__((no_instrument_function));
void __cyg_profile_func_exit  (void *this_fn, void *call_site) __attribute__((no_instrument_function));

static FILE *fp_trace;
 
void
__attribute__ ((constructor))
trace_begin (void)
{
 fp_trace = fopen("trace.out", "w");
 setvbuf(fp_trace, NULL, _IONBF, 0) ;
 counter = 0;
 instrumenting = false;
}
 
void
__attribute__ ((destructor))
trace_end (void)
{
 if(fp_trace != NULL) {
 fclose(fp_trace);
 }
}
 
void
__cyg_profile_func_enter (void *func,  void *caller)
{
  if (instrumenting) return;
 instrumenting = true;
 {
  std::lock_guard<std::mutex> lock(mymtx);
  Dl_info di;
  if (fp_trace && dladdr(func, &di)) {
    for (int i=0;i<counter;++i) fprintf(fp_trace, " ");
    std::string name = di.dli_sname ? demangle_lookup(di.dli_sname) : "<unknown>";
    fprintf(fp_trace, "e %s (%s)\n", name.c_str(), di.dli_fname);
  }
 }
 instrumenting = false;
 counter++;
}
 
void
__cyg_profile_func_exit (void *func, void *caller)
{
    if (instrumenting) return;
 instrumenting = true;
 counter--;
 {
    std::lock_guard<std::mutex> lock(mymtx);
  Dl_info di;
  if (fp_trace && dladdr(func, &di)) {
    for (int i=0;i<counter;++i) fprintf(fp_trace, " ");
    
    std::string name = di.dli_sname ? demangle_lookup(di.dli_sname) : "<unknown>";
    fprintf(fp_trace, "x %s (%s)\n", name.c_str(), di.dli_fname);
  }
 }
 instrumenting = false;
}

}
